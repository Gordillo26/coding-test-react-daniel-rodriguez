import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link, useParams } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';

import { userService } from 'services';


class UserPage extends Component {
    constructor(props) {
        super(props);        
        this.state = {
            list: [],
            id: null,
            user: {},
            isLoaded: false
        }
    }

    componentDidMount() {
        // userService.list() ... Pour remplir this.state.list
        let id = (!this.props.match.params.id ? "1" :  this.props.match.params.id);
        userService.list()
            .then(users => {
                this.setState({
                    id: id,
                    list: users,
                    isLoaded: true
                    
                })
            this.setUser(id)
        });
        
    }

    changeUser = (e) => {
        let id = e.target.value;
        this.props.history.push('/users/' + id);
        this.setUser(id)
      }

    setUser(id){
        userService.getUserById(id)
        .then(userFetched =>{
            this.setState({
                id: id,
                user: userFetched
            })
        })
    }

    getDate(dateString){
        if(dateString){
            return new Date(dateString);
        }
        if (!dateString) {
            return new Date("2021-08-25");

        }
    }

    render() {

        var {isLoaded, list, user} = this.state;
        var articles = [];
        if (user.articles) {
            articles = user.articles;
            console.log(user);
        }
        
        

        if (!isLoaded){
            return <div>Loading...</div>
        }
        if (isLoaded && user) {
            return (
                <Fragment>
                    <Helmet>
                        { title('Page secondaire') }
                    </Helmet>
    
                    <div className="user-page content-wrap">
                        <Link to="/" className="nav-arrow">
                            <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                        </Link>
    
                        <div className="users-select">
                            <h1>
                                <select className="dropBtn" onChange={this.changeUser}>
                                    { /* Liste dynamique à partir de l'API */
                                        list.map(user => {
                                            return(
                                                <option className="dropdownContent" value={user.id}>{user.name}</option>
                                            )
                                        })
                                    }
                                </select>
                            </h1>
                        </div>

    
                        <div className="infos-block">
                            <p>Occupation: <a>{user.occupation}</a></p>
                            <p>Date de naissance: <a>{new Intl.DateTimeFormat("fr", {
                                                        year: "numeric",
                                                        month: "long",
                                                        day: "2-digit"
                                                    }).format(this.getDate(user.birthdate))}</a></p>
                        </div>
    
                        <div className="articles-list">
                            { /* Liste dynamique tirée de l'utilisateur sélectionné */
                                articles.map(article => {
                                    return(
                                        <p className="articles-element">
                                            <h3>{article.name}</h3>
                                            <p>{article.content}</p>
                                        </p>
                                    )
                                })
                                
                            }
                        </div>
                    </div>
                </Fragment>
            )
        }
        
    }
}

export default UserPage;
